from __future__ import absolute_import, division, print_function
import tensorflow as tf
tf.enable_eager_execution()

import tensorflow.contrib.eager as tfe

import unicodedata
import re
import numpy as np
import os
import time
import json

from dataset import load_dataset
from model import Encoder, Decoder, loss_function
from train import train_one_epoch

from dataset import LanguageIndex

import click

print(tf.__version__)

from dataset import preprocess_sentence

class Evaluate():
    def __init__(self, input_dir='', checkpoint=''):

        # get metadata
        self.input_dir = input_dir
        self._checkpoint = json.load(open(self.input_dir + 'variables.json'))

        self.this_checkpoint = checkpoint

        self.batch_size = self._checkpoint['batch_size']
        self.embedding_dim = self._checkpoint['embedding_dim']
        self.units = self._checkpoint['units']
        self.learning_rate = self._checkpoint['learning_rate']
        self.checkpoint_dir = self.input_dir
        self.checkpoint_prefix = os.path.join(self.checkpoint_dir, 'ckpt')

        self.inp_lang = LanguageIndex('')
        self.inp_lang.idx2word = self._checkpoint['inp_lang']['idx2word']
        self.inp_lang.word2idx = self._checkpoint['inp_lang']['word2idx']
        self.targ_lang = LanguageIndex('')
        self.targ_lang.idx2word = self._checkpoint['targ_lang']['idx2word']
        self.targ_lang.word2idx = self._checkpoint['targ_lang']['word2idx']
        self.max_length_targ = self._checkpoint['max_length_targ']
        self.max_length_inp = self._checkpoint['max_length_inp']

        self.vocab_inp_size = len(self.inp_lang.word2idx)
        self.vocab_tar_size = len(self.targ_lang.word2idx)

    def get_device(self):
        (self.device, self.data_format) = ('/gpu:0', 'channels_first')
        if tfe.num_gpus() <= 0:
            (self.device, self.data_format) = ('/cpu:0', 'channels_last')

        print('Using device %s, and data format %s.' % (self.device, self.data_format))


    def model(self):

        self.encoder = Encoder(self.vocab_inp_size, self.embedding_dim, self.units, self.batch_size)
        self.decoder = Decoder(self.vocab_tar_size, self.embedding_dim, self.units, self.batch_size)
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)

        self.step_counter = tf.train.get_or_create_global_step()
        self.checkpoint = tf.train.Checkpoint(optimizer=self.optimizer, encoder=self.encoder, decoder=self.decoder, step_counter=self.step_counter)

        if self.this_checkpoint:
            print('restoring this checkpoint: ', self.this_checkpoint)
            self.checkpoint.restore(self.this_checkpoint)
        else:
            print('restoring this checkpoint: ', tf.train.latest_checkpoint(self.checkpoint_dir))
            self.checkpoint.restore(tf.train.latest_checkpoint(self.checkpoint_dir))

    def evaluate(self, sentence):
        attention_plot = np.zeros((self.max_length_targ, self.max_length_inp))

        sentence = preprocess_sentence(sentence)

        inputs = [self.inp_lang.word2idx[i] for i in sentence.split(' ')]
        inputs = tf.keras.preprocessing.sequence.pad_sequences([inputs], maxlen=self.max_length_inp, padding='post')
        inputs = tf.convert_to_tensor(inputs)

        result = ''

        hidden = [tf.zeros((1, self.units))]
        enc_out, enc_hidden = self.encoder(inputs, hidden)

        dec_hidden = enc_hidden
        dec_input = tf.expand_dims([self.targ_lang.word2idx['<start>']], 0)

        for t in range(self.max_length_targ):
            predictions, dec_hidden, attention_weights = self.decoder(dec_input, dec_hidden, enc_out)

            # storing the attention weigths to plot later on
            attention_weights = tf.reshape(attention_weights, (-1,))
            attention_plot[t] = attention_weights.numpy()

            predicted_id = tf.multinomial(tf.exp(predictions), num_samples=1)[0][0].numpy()

            result += self.targ_lang.idx2word[str(predicted_id)] + ' '

            if self.targ_lang.idx2word[str(predicted_id)] == '<end>':
                return result, sentence, attention_plot

            # the predicted ID is fed back into the model
            dec_input = tf.expand_dims([predicted_id], 0)

        return result, sentence, attention_plot


    def predict(self, sentence):
        result, sentence, attention_plot = self.evaluate(sentence)

        print('Input: {}'.format(sentence))
        print('Predicted translation: {}'.format(result))


@click.command()
@click.option("--input-dir", default='', help="input directory where the model is stored")
@click.option("--sentence", default="tengo problemas", help="question sentence")
@click.option("--ckpt", default="", help="checkpoint to load")
def execute(input_dir, sentence, ckpt):
    '''
    Train a chatbot: @UmayuxLabs
    '''

    _execute = Evaluate(
        input_dir=input_dir,
        checkpoint=ckpt
    )

    # check if there is a gpu
    _execute.get_device()

    # setup model
    _execute.model()

    # evaluate
    _execute.predict(sentence)


if __name__ == '__main__':
    execute()

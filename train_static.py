from __future__ import absolute_import, division, print_function
import tensorflow as tf
import tensorflow.contrib.eager as tfe

import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

import unicodedata
import re
import numpy as np
import os
import time

from dataset import load_dataset
from model import Encoder, Decoder, loss_function

print(tf.__version__)

(device, data_format) = ('/gpu:0', 'channels_first')
if tfe.num_gpus() <= 0:
    (device, data_format) = ('/cpu:0', 'channels_last')

print('Using device %s, and data format %s.' % (device, data_format))


# Download the file
path_to_zip = tf.keras.utils.get_file(
    'tweets.tsv', origin='https://bench.cs.vt.edu/ftp/data/gustavo1/tweets.tsv',
    extract=False)

path_to_file = os.path.dirname(path_to_zip) + "/tweets.tsv"

# Try experimenting with the size of that dataset
num_examples = 100
input_tensor, target_tensor, inp_lang, targ_lang, max_length_inp, max_length_targ = load_dataset(path_to_file, num_examples)

# Creating training and validation sets using an 80-20 split
input_tensor_train, input_tensor_val, target_tensor_train, target_tensor_val = train_test_split(input_tensor, target_tensor, test_size=0.2)

# Create a TF dataset
BUFFER_SIZE = len(input_tensor_train)
BATCH_SIZE = 64
N_BATCH = BUFFER_SIZE//BATCH_SIZE
embedding_dim = 256 #TODO use transfer learning
units = 1024
vocab_inp_size = len(inp_lang.word2idx)
vocab_tar_size = len(targ_lang.word2idx)

dataset = tf.data.Dataset.from_tensor_slices((input_tensor_train, target_tensor_train)).shuffle(BUFFER_SIZE)
dataset = dataset.apply(tf.contrib.data.batch_and_drop_remainder(BATCH_SIZE))

encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
decoder = Decoder(vocab_tar_size, embedding_dim, units, BATCH_SIZE)

optimizer = tf.train.AdamOptimizer()

summary_writer = tf.contrib.summary.create_file_writer('./epoch/', flush_millis=10000)
checkpoint_prefix = os.path.join('./ckpt/', 'ckpt')


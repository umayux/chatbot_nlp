import tensorflow as tf

def train_one_epoch(encoder, decoder, dataset, targ_lang, optimizer, loss_function, batch_size):

    hidden = encoder.initialize_hidden_state()

    total_loss = 0

    for (batch, (inp, targ)) in enumerate(dataset):
        loss = 0
        # with tf.contrib.summary.record_summaries_every_n_global_steps(1, global_step=tf.train.get_global_step()):

        with tf.GradientTape() as tape:
            enc_output, enc_hidden = encoder(inp, hidden)
            dec_hidden = enc_hidden
            dec_input = tf.expand_dims([targ_lang.word2idx['<start>']] * batch_size, 1)

            # Teacher forcing - feeding the target as the next input
            for t in range(1, targ.shape[1]):
                # passing enc_output to the decoder
                predictions, dec_hidden, _ = decoder(dec_input, dec_hidden, enc_output)
                loss += loss_function(targ[:, t], predictions)

                # using teacher forcing
                dec_input = tf.expand_dims(targ[:, t], 1)

                # storing summary
                # tf.contrib.summary.scalar('loss', loss)

        batch_loss = (loss / int(targ.shape[1]))
        total_loss += batch_loss
        variables = encoder.variables + decoder.variables
        gradients = tape.gradient(loss, variables)
        optimizer.apply_gradients(zip(gradients, variables), tf.train.get_global_step())


    return total_loss
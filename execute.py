from __future__ import absolute_import, division, print_function
import tensorflow as tf

tf.enable_eager_execution()
tf.executing_eagerly()
tfe = tf.contrib.eager

import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

import unicodedata
import re
import numpy as np
import os
import time
import json
from dataset import load_dataset
from model import Encoder, Decoder, loss_function
from train import train_one_epoch
from dataset import LanguageIndex
import click

print(tf.__version__)

from dataset import preprocess_sentence

class Execute():
    def __init__(self, input_file='', num_examples=20000, batch_size=50, embedding_dim=256, units=1024, learning_rate=0.001, output_dir='', epochs=10, retrain=False, this_checkpoint=''):
        self.input_file = input_file
        self.num_examples = num_examples
        self.batch_size = batch_size
        self.embedding_dim = embedding_dim
        self.units = units
        self.learning_rate = learning_rate
        self.output_dir = output_dir
        self.epochs = epochs
        self.checkpoint_dir = self.output_dir + '/ckpt/'
        self.train_dir = self.output_dir + '/train/'
        self.checkpoint_prefix = os.path.join(self.checkpoint_dir, 'ckpt')
        self.retrain = retrain
        self.this_checkpoint = this_checkpoint


    def download_dataset(self, url='https://bench.cs.vt.edu/ftp/data/gustavo1/tweets.tsv'):
        # Download the file
        path_to_zip = tf.keras.utils.get_file(
            'tweets.tsv', origin=url,
            extract=False)

        self.input_file = os.path.dirname(path_to_zip) + "/tweets.tsv"


    def build_dataset(self, test_size=0.2):

        if self.retrain:

            # get metadata
            _checkpoint = json.load(open(self.output_dir + '/ckpt/variables.json'))

            # update langague metadata
            self.inp_lang = LanguageIndex('')
            self.inp_lang.idx2word = _checkpoint['inp_lang']['idx2word']
            self.inp_lang.word2idx = _checkpoint['inp_lang']['word2idx']
            self.targ_lang = LanguageIndex('')
            self.targ_lang.idx2word = _checkpoint['targ_lang']['idx2word']
            self.targ_lang.word2idx = _checkpoint['targ_lang']['word2idx']

            # Try experimenting with the size of that dataset
            self.input_tensor, self.target_tensor, self.inp_lang, self.targ_lang, self.max_length_inp, self.max_length_targ = load_dataset(
                self.input_file,
                self.num_examples,
                retrain=self.retrain,
                max_length_inp=_checkpoint['max_length_inp'],
                max_length_tar=_checkpoint['max_length_targ'],
                inp_lang=self.inp_lang,
                targ_lang=self.targ_lang
            )

        else:
            self.input_tensor, self.target_tensor, self.inp_lang, self.targ_lang, self.max_length_inp, self.max_length_targ = load_dataset(self.input_file, self.num_examples)


        # Creating training and validation sets using an 80-20 split
        self.input_tensor_train, self.input_tensor_val, self.target_tensor_train, self.target_tensor_val = train_test_split(self.input_tensor, self.target_tensor, test_size=test_size)

        self.buffer_size = len(self.input_tensor_train)
        self.n_batch = self.buffer_size//self.batch_size
        self.vocab_inp_size = len(self.inp_lang.word2idx)
        self.vocab_tar_size = len(self.targ_lang.word2idx)

        self.dataset = tf.data.Dataset.from_tensor_slices((self.input_tensor_train, self.target_tensor_train)).shuffle(self.buffer_size)
        self.dataset = self.dataset.apply(tf.contrib.data.batch_and_drop_remainder(self.batch_size))


    def model(self):

        tf.gfile.MakeDirs(self.checkpoint_dir)

        self.encoder = Encoder(self.vocab_inp_size, self.embedding_dim, self.units, self.batch_size)
        self.decoder = Decoder(self.vocab_tar_size, self.embedding_dim, self.units, self.batch_size)
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)

        self.checkpoint = tfe.Checkpoint(optimizer=self.optimizer, encoder=self.encoder, decoder=self.decoder)

        if self.this_checkpoint:
            print("loading customized checkpoint %s" %(self.this_checkpoint))
            self.checkpoint.restore(self.this_checkpoint)
        else:
            print("loading latest checkpoint %s" %(tf.train.latest_checkpoint(self.checkpoint_dir)))
            self.checkpoint.restore(tf.train.latest_checkpoint(self.checkpoint_dir))

    def train(self):

        for epoch in range(0, self.epochs):

            self.global_step = tf.train.get_or_create_global_step()
            start = time.time()

            total_loss = train_one_epoch(self.encoder, self.decoder, self.dataset, self.targ_lang, self.optimizer, loss_function, self.batch_size)

            end = time.time()
            print('\nTrain time for epoch #%d (%d total steps): %f total loss: %f' % (
                self.checkpoint.save_counter.numpy() + 1,
                self.global_step.numpy(),
                end - start,
                total_loss
            ))

            self.checkpoint.save(file_prefix=self.checkpoint_prefix)

    def save_variables(self):
        data = {
            'inp_lang': {'word2idx': self.inp_lang.word2idx, 'idx2word':self.inp_lang.idx2word},
            'targ_lang': {'word2idx': self.targ_lang.word2idx, 'idx2word':self.targ_lang.idx2word},
            'max_length_inp': self.max_length_inp,
            'max_length_targ': self.max_length_targ,
            'vocab_inp_size': self.vocab_inp_size,
            'vocab_tar_size': self.vocab_tar_size,
            'embedding_dim': self.embedding_dim,
            'learning_rate': self.learning_rate,
            'batch_size': self.batch_size,
            'units': self.units
        }

        json.dump(data, open(self.output_dir + '/ckpt/variables.json', 'w'))




@click.command()
@click.option("--input-file", default='', help="input tabular file with format: [Question, Answer]")
@click.option("--num-examples", default=20000, help="number of items to use for training out of the total number of items in the input dataset (default: 20000)")
@click.option("--batch-size", default=50, help="batch size (default: 50)")
@click.option("--embedding-dim", default=256, help="embedding dimension (default: 256)")
@click.option("--units", default=1024, help="units (default: 1024)")
@click.option("--learning-rate", default=0.001, help="learning rate")
@click.option("--output-dir", default='', help="output directory")
@click.option("--epochs", default=10, help="number of epochs for training")
@click.option("--data-online", default=False, help="Download the data from a given URL (default: False)")
@click.option("--url", default='', help="URL where the data is hosted, only if --data-online is enabled")
@click.option("--retrain", is_flag=True, help="Retrain the model. This option enables to continue training the model. To start from the begining remove the ./ckpt/ file and do the training (default: False)")
@click.option("--ckpt", default='', help="Restart from this checkpoint")
def execute(input_file, num_examples, batch_size, embedding_dim, units, learning_rate, output_dir, epochs, data_online, url, retrain, ckpt):
    '''

    Train a chatbot: @UmayuxLabs

    '''

    _execute = Execute(
        input_file=input_file,
        num_examples=num_examples,
        batch_size=batch_size,
        embedding_dim=embedding_dim,
        units=units,
        learning_rate=learning_rate,
        output_dir=output_dir,
        epochs=epochs,
        retrain=retrain,
        this_checkpoint=ckpt
    )

    # check if there is a gpu
    # _execute.get_device()

    # download dataset
    if data_online:
        _execute.download_dataset(url=url)

    # build the dataset
    _execute.build_dataset(test_size=0.2)

    # setup model
    _execute.model()

    # store global variables
    if not retrain:
        _execute.save_variables()

    # train the model
    _execute.train()






if __name__ == '__main__':
    execute()

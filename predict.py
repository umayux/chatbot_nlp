
from index import LanguageIndex
import json

import tensorflow as tf
import tensorflow.contrib.eager as tfe

tf.enable_eager_execution()

import unicodedata
import re
import numpy as np
import os
import time

from dataset import load_dataset
from model import Encoder, Decoder, loss_function
from dataset import preprocess_sentence

from utils import evaluate, plot_attention, translate

print(tf.__version__)

_checkpoint = json.load(open('./ckpt/variables.json'))
inp_lang = LanguageIndex('')
inp_lang.idx2word = _checkpoint['inp_lang']['idx2word']
inp_lang.word2idx = _checkpoint['inp_lang']['word2idx']
targ_lang = LanguageIndex('')
targ_lang.idx2word = _checkpoint['targ_lang']['idx2word']
targ_lang.word2idx = _checkpoint['targ_lang']['word2idx']
learning_rate = _checkpoint['learning_rate']
max_length_inp = _checkpoint['max_length_inp']
max_length_targ = _checkpoint['max_length_targ']

BUFFER_SIZE = _checkpoint['learning_rate']
BATCH_SIZE = _checkpoint['batch_size']
N_BATCH = BUFFER_SIZE//BATCH_SIZE
embedding_dim = _checkpoint['embedding_dim']
units = _checkpoint['units']
vocab_inp_size = len(inp_lang.word2idx)
vocab_tar_size = len(targ_lang.word2idx)


encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
decoder = Decoder(vocab_tar_size, embedding_dim, units, BATCH_SIZE)
optimizer = tf.train.AdamOptimizer(learning_rate=0.001)

output_dir = '.'
checkpoint_dir = output_dir + '/ckpt/'

checkpoint_prefix = os.path.join(checkpoint_dir, 'ckpt')
step_counter = tf.train.get_or_create_global_step()
checkpoint = tf.train.Checkpoint(optimizer=optimizer, encoder=encoder, decoder=decoder, step_counter=step_counter)

print('loading this checkpoint: ', tf.train.latest_checkpoint(checkpoint_dir))
checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

(device, data_format) = ('/gpu:0', 'channels_first')
if tfe.num_gpus() <= 0:
    (device, data_format) = ('/cpu:0', 'channels_last')

print('Using device %s, and data format %s.' % (device, data_format))

with tf.device(device):
    translate('no puede ser, ustedes son unos ladrones', encoder, decoder, inp_lang, targ_lang, max_length_inp, max_length_targ, units)
